--[[***********************************************

    MENDELOVA UNIVERZITA V BRNE
    ÚVIS AVC
    Michal Mallo

***************************************************

    Info.lua
    Summary information about plugin.

***************************************************
]]

return {
    LrSdkVersion = 3.0,
    LrSdkMinimumVersion = 1.3,
    LrToolkitIdentifier = 'cz.mendelu.uvis.sdk.facerecognition',
    LrPluginName = LOC "$$$/FaceRecognition/PluginName=Face Recognition";
    LrLibraryMenuItems = {
        {
        title = LOC "$$$/FaceRecognition/CustomDialog=Face Recognition Modul",
        file = "FaceRecognitionDialog.lua",
        enabledWhen = 'photosSelected',
        },
    },
    VERSION = { major=1, minor=0, revision=0, build="20210221-development", }
}