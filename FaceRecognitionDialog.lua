--[[***********************************

    MENDELOVA UNIVERZITA V BRNE
    ÚVIS AVC
    Michal Mallo

***************************************

    FaceRecognitionDialog.lua

    Basic dialog window for plugin.

***************************************
]]
-- Prístup ku SDK namespaces
local LrFunctionContext = import 'LrFunctionContext'
local LrLogger = import 'LrLogger'
local LrFileUtils = import 'LrFileUtils'
local LrPathUtils = import 'LrPathUtils'
local LrBinding = import 'LrBinding'
local LrTasks = import 'LrTasks'
local LrDevelopController = import 'LrDevelopController'
local LrDialogs = import 'LrDialogs'
local LrView = import 'LrView'
local LrApplication = import 'LrApplication'
local LrColor = import 'LrColor'
local LrProgressScope = import 'LrProgressScope'
local catalog = import("LrApplication").activeCatalog()
local bind = LrView.bind
--local LrFolder = import 'LrFolder'

--logger pre debugovanie v aplikácii
local myLogger = LrLogger('libraryLogger')
      myLogger:enable("print")

local function outputToLog(message)
    myLogger:trace(message)
end
local writeCrop

-- tabulka s výberom orezania
local aspectRatios = {  {title="automaticky", value = 1},
                        {title="1 x 1", value = 2},
                        {title="2 x 3", value = 3},
                        {title="3 x 5", value = 4},
                        {title="8 x 10", value = 5}}

--tabulka s krajnamy
local countryDatabase = {       {title ="Průkazové foto",  value = "1"}, 
                                {title ="Albánsko",  value = "2"}, 
                                {title ="Austrália",  value = "3"}, 
                                {title ="Azerbajdžan",  value = "4"}, 
                                {title ="Belgicko",  value = "5"}, 
                                {title ="Bielorusko",  value = "6"}, 
                                {title ="Bosna a Hercegovina",  value = "7"}, 
                                {title ="Brazília",  value = "8"}, 
                                {title ="Bulharsko",  value = "9"}, 
                                {title ="Cyprus",  value = "10"}, 
                                {title ="Česká republika",  value = "11"}, 
                                {title ="Čierna Hora",  value = "12"}, 
                                {title ="Čína",  value = "13"}, 
                                {title ="Čína e-víza",  value = "14"}, 
                                {title ="Dánsko",  value = "15"}, 
                                {title ="Ekvádor",  value = "16"}, 
                                {title ="El Salvádor",  value = "17"}, 
                                {title ="Estonsko",  value = "18"}, 
                                {title ="Etiópia",  value = "19"}, 
                                {title ="Filipíny",  value = "20"}, 
                                {title ="Fínsko",  value = "21"}, 
                                {title ="Francúzko",  value = "22"}, 
                                {title ="Ghana",  value = "23"}, 
                                {title ="Grécko",  value = "24"}, 
                                {title ="Gruzínsko",  value = "25"}, 
                                {title ="Holandsko",  value = "26"}, 
                                {title ="Chile",  value = "27"}, 
                                {title ="Chorvátsko",  value = "28"}, 
                                {title ="India",  value = "29"}, 
                                {title ="Irán",  value = "30"}, 
                                {title ="Írsko",  value = "31"}, 
                                {title ="Izrael",  value = "32"}, 
                                {title ="Japonsko",  value = "33"}, 
                                {title ="JAR",  value = "34"}, 
                                {title ="Jemen",  value = "35"}, 
                                {title ="Jordánsko",  value = "36"}, 
                                {title ="Južná kórea",  value = "37"}, 
                                {title ="Kambodža",  value = "38"}, 
                                {title ="Kazachstan",  value = "39"}, 
                                {title ="Kolumbia",  value = "40"}, 
                                {title ="Konžská demokratická republika",  value = "41"}, 
                                {title ="Kosovo",  value = "42"}, 
                                {title ="Kurgistán",  value = "43"}, 
                                {title ="Kyrgystan",  value = "44"}, 
                                {title ="Litva",  value = "45"}, 
                                {title ="Lotyšsko",  value = "46"}, 
                                {title ="Macedónsko",  value = "47"}, 
                                {title ="Maďarsko",  value = "48"}, 
                                {title ="Mexiko",  value = "49"}, 
                                {title ="Moldavsko",  value = "50"}, 
                                {title ="Mongolsko",  value = "51"}, 
                                {title ="Mozambik",  value = "52"}, 
                                {title ="Nemecko",  value = "53"}, 
                                {title ="Nigerie",  value = "54"}, 
                                {title ="Nikaragua",  value = "55"}, 
                                {title ="Nórsko",  value = "56"}, 
                                {title ="Peru",  value = "57"}, 
                                {title ="Poľsko",  value = "58"}, 
                                {title ="Portugalsko",  value = "59"}, 
                                {title ="Rakúsko",  value = "60"}, 
                                {title ="Rumunsko",  value = "61"}, 
                                {title ="Rusko",  value = "62"}, 
                                {title ="Slovensko",  value = "63"}, 
                                {title ="Slovinsko",  value = "64"}, 
                                {title ="Srbsko",  value = "65"}, 
                                {title ="Španielsko",  value = "66"}, 
                                {title ="Švédsko",  value = "67"}, 
                                {title ="Taliansko",  value = "68"}, 
                                {title ="Tazmánia",  value = "69"}, 
                                {title ="Thajsko",  value = "70"}, 
                                {title ="Turecko",  value = "71"}, 
                                {title ="Uganda",  value = "72"}, 
                                {title ="Ukrajna",  value = "73"}, 
                                {title ="USA",  value = "74"}, 
                                {title ="Veľká Británia",  value = "75"}, 
                                {title ="Zambia",  value = "76"}}






detectedValues = {}
--cD = {{w=35,h=45,ppi=0,hH=31.5,tM=3,bE=0,wF=0,bCh=0,fA=0,hA=0,tA=0}}

--tabuľka krajín 
local photoCountryParams = {{country=1,  width=20,  height=25,  ppi=300,  headHeight=18,  topMargin=1.6,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=2,  width=36,  height=47,  ppi=600,  headHeight=34.5,  topMargin=4,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=3,  width=35,  height=45,  ppi=600,  headHeight=33,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=4,  width=35,  height=45,  ppi=600,  headHeight=34,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=5,  width=35,  height=45,  ppi=600,  headHeight=31,  topMargin=2.5,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=6,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=7,  width=35,  height=45,  ppi=600,  headHeight=34.6,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=8,  width=431,  height=531,  ppi=600,  headHeight=0,  topMargin=0,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=70,  bottomToEyeArea=0,  topMarginArea=44232,  color="white"}, 
    {country=9,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=10,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=11,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=12,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=13,  width=33,  height=48,  ppi=600,  headHeight=30,  topMargin=4,  bottomToEye=0,  faceWidth=17,  bottomToChin=8,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=14,  width=420,  height=560,  ppi=600,  headHeight=85,  topMargin=0,  bottomToEye=256,  faceWidth=230,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=15,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=16,  width=50,  height=50,  ppi=305,  headHeight=0,  topMargin=0,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=68,  bottomToEyeArea=56,  topMarginArea=0,  color="white"}, 
    {country=17,  width=51,  height=51,  ppi=300,  headHeight=0,  topMargin=0,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=65,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=18,  width=40,  height=50,  ppi=600,  headHeight=0,  topMargin=0,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=75,  bottomToEyeArea=0,  topMarginArea=6,  color="grey"}, 
    {country=19,  width=30,  height=40,  ppi=600,  headHeight=27,  topMargin=2.5,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=20,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=21,  width=36,  height=47,  ppi=353,  headHeight=35,  topMargin=4,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=22,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=23,  width=35,  height=45,  ppi=600,  headHeight=30,  topMargin=3,  bottomToEye=31,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=24,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=25,  width=35,  height=45,  ppi=600,  headHeight=0,  topMargin=5,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=69,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=26,  width=35,  height=45,  ppi=600,  headHeight=29,  topMargin=6.5,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=27,  width=20,  height=30,  ppi=0,  headHeight=0,  topMargin=0,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=75,  bottomToEyeArea=0,  topMarginArea=10,  color="white"}, 
    {country=28,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=29,  width=51,  height=51,  ppi=300,  headHeight=32.766,  topMargin=29.972,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=30,  width=400,  height=600,  ppi=600,  headHeight=0,  topMargin=7,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=70,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=31,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=32,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=33,  width=45,  height=45,  ppi=600,  headHeight=27,  topMargin=7,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=34,  width=35,  height=45,  ppi=600,  headHeight=0,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=70,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=35,  width=40,  height=60,  ppi=300,  headHeight=38,  topMargin=6,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=36,  width=35,  height=45,  ppi=300,  headHeight=33,  topMargin=4,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=37,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=38,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=39,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=40,  width=30,  height=40,  ppi=600,  headHeight=30,  topMargin=2.5,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=41,  width=35,  height=45,  ppi=600,  headHeight=31,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=42,  width=51,  height=51,  ppi=300,  headHeight=37,  topMargin=0,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=43,  width=51,  height=51,  ppi=300,  headHeight=32.766,  topMargin=29.972,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=44,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=45,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=46,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=47,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=48,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=49,  width=25,  height=35,  ppi=600,  headHeight=26,  topMargin=1.7,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=50,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=51,  width=30,  height=40,  ppi=600,  headHeight=30,  topMargin=2.5,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=52,  width=35,  height=45,  ppi=600,  headHeight=34,  topMargin=0,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=53,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=54,  width=35,  height=45,  ppi=600,  headHeight=34,  topMargin=0,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=55,  width=51,  height=51,  ppi=300,  headHeight=0,  topMargin=0,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=65,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=56,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=57,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=4.5,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=58,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=59,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=60,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=61,  width=30,  height=40,  ppi=600,  headHeight=30,  topMargin=2.5,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=62,  width=35,  height=45,  ppi=600,  headHeight=33,  topMargin=5,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=63,  width=30,  height=35,  ppi=600,  headHeight=25,  topMargin=2,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=64,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=65,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=66,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=67,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=68,  width=35,  height=45,  ppi=600,  headHeight=34.5,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=69,  width=35,  height=45,  ppi=600,  headHeight=33,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=70,  width=35,  height=45,  ppi=600,  headHeight=44347,  topMargin=3,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=71,  width=50,  height=60,  ppi=300,  headHeight=34,  topMargin=10,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=72,  width=51,  height=51,  ppi=300,  headHeight=32.766,  topMargin=29.972,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=73,  width=30,  height=40,  ppi=600,  headHeight=31,  topMargin=2.5,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=74,  width=51,  height=51,  ppi=300,  headHeight=30,  topMargin=30,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}, 
    {country=75,  width=35,  height=45,  ppi=600,  headHeight=32.8,  topMargin=4.9,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="grey"}, 
    {country=76,  width=35,  height=45,  ppi=600,  headHeight=34,  topMargin=0,  bottomToEye=0,  faceWidth=0,  bottomToChin=0,  faceArea=0,  bottomToEyeArea=0,  topMarginArea=0,  color="white"}}

                    
-- funkcia hlavného zobrazovacieho okna
local function showCustomDialog()
        
        LrFunctionContext.callWithContext( "showCustomDialog", function(context)

            -- Načítanie označenej fotografie
            local selectedPhotos = catalog.targetPhotos 
            local photo = catalog.targetPhoto 
            local f = LrView.osFactory()
            local props = LrBinding.makePropertyTable( context )
            props.selectedButton = "one"
            props.countryValue = "1"
            props.ratioValue = "1"
            props.widthValue = "0"
            props.heightValue = "0"
            props.dpiValue = "0"
            props.headHeightValue = "0"
            props.topMarginValue = "0"
            props.bottomEyeValue = "0"
            props.faceWidthValue = "0"
            props.bottomChinValue = "0"
            props.faceAreaValue = "0"
            props.bottomEyeAreaValue = "0"
            props.headTopAreaValue = "0"



            -- *** 3 riadok *** V dialógu je to popup menu KRAJNY
            local popupCountry = f:popup_menu{
                alignment = "left",
                items = countryDatabase,
                
                value = props.countryValue,
                visible = LrView.bind(
                {
                    key = "selectedButton", 
                    transform = function( value, _ )
                        if value == "one" then
                            return true
                        else
                            return false
                        end
                    end
                }),
            }

            -- **** 4 riadok ****  V dialógu to je popup menu POMER STRAN
            local popupRatio = f:popup_menu{
                items = aspectRatios,
                value = props.ratioValue,
                visible = LrView.bind(
                {
                    key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                    transform = function( value, _ )
                        if value == "one" then
                            return false
                        else
                            return true
                        end
                    end
                }),
            }

            -- ***** 5 riadok ***** Nastevenie hodnoty ŠÍRKA FOTOGRAFIE 
            local setWidth = f:edit_field{
                width_in_chars = 3,
                immediate = false,
                value = props.widthValue,
                visible = LrView.bind(
                    {
                        key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                        transform = function( value, _ )
                            if value == "one" then
                                return false
                            else
                                return true
                            end
                        end
                    }),
                
            }

            -- ****** 6 riadok ****** Nastevenie hodnoty VÝŠKA FOTOGRAFIE
            local setHeight = f:edit_field{
                width_in_chars = 3,
                immediate = false,
                value = props.heightValue,
                visible = LrView.bind(
                    {
                        key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                        transform = function( value, _ )
                            if value == "one" then
                                return false
                            else
                                return true
                            end
                        end
                    }),
            }

            -- ******* 7 riadok ******* Nastevenie hodnoty: ROZLÍŠENIE FOTOGRAFIE
            local setDPI = f:edit_field{
                width_in_chars = 3,
                immediate = false,
                value = props.dpiValue,
                visible = LrView.bind(
                    {
                        key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                        transform = function( value, _ )
                            if value == "one" then
                                return false
                            else
                                return true
                            end
                        end
                    }),
            }

            -- ******** 8 riadok ******** Nastevenie hodnoty: VÝŠKA TVÁRE - BRADA -> VLASY
            local setHeadHeight =f:edit_field{
                width_in_chars = 3,
                immediate = false,
                value = props.headHeightValue,
                visible = LrView.bind(
                    {
                        key = "selectedButton",
                        transform = function( value, _ )
                            if value == "one" then
                                return false
                            else
                                return true
                            end
                        end
                    }),
            }

             -- ********* 9 riadok ********* Nastevenie hodnoty: VZDIALENOSŤ - HLAVA -> HORNÝ KRAJ
            local setTopMargin = f:edit_field{
                width_in_chars = 3,
                immediate = false,
                value = props.topMarginValue,
                visible = LrView.bind(
                    {
                        key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                        transform = function( value, _ )
                            if value == "one" then
                                return false
                            else
                                return true
                            end
                        end
                    }),
            }

            -- ********** 10 riadok ********** Nastevenie hodnoty: VZDIALENOSŤ - LÍNIA OČÍ -> DOLNÝ KRAJ
            local setBottomEyeHeight = f:edit_field{
                width_in_chars = 3,
                immediate = false,
                value = props.bottomEyeValue,
                visible = LrView.bind(
                    {
                        key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                        transform = function( value, _ )
                            if value == "one" then
                                return false
                            else
                                return true
                            end
                        end
                    }),
            }

            -- *********** 11 riadok *********** Nastevenie hodnoty: VZDIALENOSŤ - ĽAVÝ KRAJ TVÁRE -> PRAVÝ KRAJ TVÁRE
            local setFaceWidth = f:edit_field{
                width_in_chars = 3,
                immediate = false,
                value = props.faceWidthValue,
                visible = LrView.bind(
                    {
                        key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                        transform = function( value, _ )
                            if value == "one" then
                                return false
                            else
                                return true
                            end
                        end
                    }),
            }

            -- ************ 12 riadok ************ Nastevenie hodnoty: VZDIALENOSŤ - DOLNÝ KRAJ -> BRADA
            local setBottomChinHeight = f:edit_field{
                width_in_chars = 3,
                immediate = false,
                value = props.bottomChinValue,
                visible = LrView.bind(
                    {
                        key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                        transform = function( value, _ )
                            if value == "one" then
                                return false
                            else
                                return true
                            end
                        end
                    }),
            }

            -- ************* 13 riadok ************* Nastevenie hodnoty: PLOCHA HLAVY
            local setFaceArea = f:edit_field{
                immediate = false,
                width_in_chars = 3,
                value = props.faceAreaValue,
                visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
            }

            -- ************** 14 riadok ************** Nastevenie hodnoty: PLOCHA DOLNÝ KRAJ -> LÍNIA OČÍ 
            local setBottomEyeArea = f:edit_field{
                immediate = false,
                width_in_chars = 3,
                value = props.bottomEyeAreaValue,
                visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
            }

            -- *************** 15 riadok *************** Nastevenie hodnoty: PLOCHA NAD HLAVOU PO OKRAJ 
            local setHeadTopArea = f:edit_field{
                width_in_chars = 3,
                immediate = false,
                value = props.headTopAreaValue,
                visible = LrView.bind(
                    {
                        key = "selectedButton", 
                        transform = function( value, _ )
                            if value == "one" then
                                return false
                            else
                                return true
                            end
                        end
                    }),
            }

            -- Vloženie názvov fotografií z metadát do tabuľky
            quantity = 0
            photoNames = {}
            folderNames = {}
            for i in pairs( selectedPhotos ) do
                local pName = selectedPhotos[i]:getFormattedMetadata('fileName')
                folderName = selectedPhotos[i]:getRawMetadata('path')
                quantity = quantity + 1
                photoNames[i] = pName  
                folderNames[i] = folderName
            end

        

            -- Výpis do stĺpcov a riadkov 
            local c = f:column{
                bindToObject = props,
                spacing = f:label_spacing(),
                
                -- * 1 riadok * Radio buttons, výber medzi automatickým a manuálnym zadaním
                f:row{
                    f:column{
                        spacing = f:control_spacing(),
                        alignment = "right",
                        -- A radio button is selected when its value is equal to its checked_value
                        f:radio_button {
                            title = "Automaticky nastaviť podľa krajiny",
                            value = LrView.bind( "selectedButton" ),
                            checked_value = "one",
                        },
                        f:radio_button {
                            title = "Manuálne nastaviť hodnoty",
                            value = LrView.bind( "selectedButton" ),
                            checked_value = "two",
                        },
                    },
                }, 

                -- ** 2 riadok ** Výpis počtu označených fotografií 
                f:row{
                    f:column{
                        f:static_text {
                            alignment = "left",
                            width = LrView.share "label_width",
                            text_color = LrColor(0,0,1),
                            title ="Označené fotografie:    " .. quantity,
                        },
                        f:separator{
                            fill_horizontal = 1,
                        },
                    },
                },     

                -- *** 3 riadok *** Výber podľa krajiny pre automatické nastavenie
                f:row{
                    f:column{
                        f:static_text{
                            height_in_lines = 1,5,
                            alignment = "left",
                            width = LrView.share "label_width",
                            title = "Výber podľa krajiny: ",
                            visible = LrView.bind(
                            {
                                key = "selectedButton",
                                transform = function( value, _ )
                                    if value == "one" then
                                        return true
                                    else
                                        return false
                                    end
                                end
                            }),
                        },
                        
                        popupCountry,
                    },
                    
                    
                        
                },      

                --[[ **** 4 riadok **** Nastavenie POMERU STRAN, výber z popup menu
                f:row{
                    f:column{
                        f:static_text{
                            alignment = "right",
                            width = LrView.share "label_width",
                            title = "Pomer strán: ",
                            visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                        },
                    },
                    f:column{
                        fill_horizontal = 1,
                        popupRatio, 
                    },
                        
                },]]

                -- ***** 5 riadok ***** Nastevenie hodnoty: ŠÍRKA FOTOGRAFIE 
                f:row{
                    f:column{
                        f:static_text{
                            alignment = "right",
                            width = LrView.share "label_width",
                            title = "Šírka fotografie: ",
                            visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                        },
                    },
                    setWidth,

                    f:static_text{
                        alignment = "center",
                        title = "mm",
                        visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                    },
                        
                },

                -- ****** 6 riadok ****** Nastevenie hodnoty: VÝŠKA FOTOGRAFIE 
                f:row{
                    visible="true",
                    f:column{
                        f:static_text{
                            alignment = "right",
                            width = LrView.share "label_width",
                            title = "Výška fotografie: ",
                            visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                        },
                    },
                    setHeight,
                    f:static_text{
                        alignment = "center",
                        title = "mm",
                        visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                    }
                        
                },
                --[[ ******* 7 riadok ******* Nastevenie hodnoty: ROZLÍŠENIE FOTOGRAFIE
                f:row{
                    f:column{
                        f:static_text{
                            alignment = "right",
                            width = LrView.share "label_width",
                            title = "Rozlíšenie fotografie: ",
                            visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                        },
                    },
                    setDPI,

                    f:static_text{
                        alignment = "center",
                        title = "PPI",
                        visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                    }
                        
                },]]

                -- ******** 8 riadok ******** Nastevenie hodnoty: VÝŠKA TVÁRE - BRADA -> VLASY
                f:row{
                    f:column{
                        f:static_text{
                            alignment = "right",
                            width = LrView.share "label_width",
                            title = "Výška tváre od brady až po hornú časť vlasov: ",
                            visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                        },
                    },
                    setHeadHeight,

                    f:static_text{
                        alignment = "center",
                        title = "mm",
                        visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                    }
                        
                },


                -- ********* 9 riadok ********* Nastevenie hodnoty: VZDIALENOSŤ - HLAVA -> HORNÝ KRAJ
                f:row{
                    f:column{
                        f:static_text{
                            width_in_chars = 3,
                            alignment = "right",
                            width = LrView.share "label_width",
                            title = "Vzdialenosť od hlavy po horný okraj: ",
                            visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                        },
                    },
                    setTopMargin,

                    f:static_text{
                        alignment = "center",
                        title = "mm",
                        visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                    }
                        
                },


                -- ********** 10 riadok ********** Nastevenie hodnoty: VZDIALENOSŤ - LÍNIA OČÍ -> DOLNÝ KRAJ
                f:row{
                    f:column{
                        f:static_text{
                            alignment = "right",
                            width = LrView.share "label_width",
                            title = "Vzdialenosť očí od dolnej časti fotografie: ",
                            visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                        },
                    },
                    setBottomEyeHeight,

                    f:static_text{
                        alignment = "center",
                        title = "mm",
                        visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                    }
                        
                },

               
                -- *********** 11 riadok *********** Nastevenie hodnoty: VZDIALENOSŤ - ĽAVÝ KRAJ TVÁRE -> PRAVÝ KRAJ TVÁRE
                f:row{
                    f:column{
                        f:static_text{
                            alignment = "right",
                            width = LrView.share "label_width",
                            title = "Vzdialenosť od lavej časti tváre po pravú bez uší: ",
                            visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                        },
                    },
                    setFaceWidth,

                    f:static_text{
                        alignment = "center",
                        title = "mm",
                        visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                    }
                        
                },


                -- ************ 12 riadok ************ Nastevenie hodnoty: VZDIALENOSŤ - DOLNÝ KRAJ -> BRADA
                f:row{
                    f:column{
                        f:static_text{
                            alignment = "right",
                            width = LrView.share "label_width",
                            title = "Vzdialenosť od spodnej časti fotografie po bradu: ",
                            visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                        },
                    },
                    setBottomChinHeight,

                    f:static_text{
                        alignment = "center",
                        title = "mm",
                        visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                    }
                        
                },

                 -- ************* 13 riadok ************* Nastevenie hodnoty: PLOCHA HLAVY
                f:row{
                    f:column{
                        f:static_text{
                            alignment = "right",
                            width = LrView.share "label_width",
                            title = "Plocha zabratá hlavou: ",
                            visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                        },
                    },
                    setFaceArea,

                    f:static_text{
                        alignment = "center",
                        title = "%",
                        visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                    }
                        
                },

                 -- ************** 14 riadok ************** Nastevenie hodnoty: PLOCHA DOLNÝ KRAJ -> LÍNIA OČÍ 
                 f:row{
                    f:column{
                        f:static_text{
                            alignment = "right",
                            width = LrView.share "label_width",
                            title = "Plocha od dolnej časti fotografie po líniu očí: ",
                            visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                        },
                    },
                    setBottomEyeArea,  

                    f:static_text{
                        alignment = "center",
                        title = "%",
                        visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                    }
                        
                },     
                 
                -- *************** 15 riadok *************** Nastevenie hodnoty: PLOCHA NAD HLAVOU PO OKRAJ 
                 f:row{
                    f:column{
                        f:static_text{
                            alignment = "right",
                            width = LrView.share "label_width",
                            title = "Plocha zabratá nad hlavou po okraj: ",
                            visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                        },
                    },
                    setHeadTopArea,

                    f:static_text{
                        alignment = "center",
                        title = "%",
                        visible = LrView.bind(
                            {
                                key = "selectedButton", -- the key value to bind to.  The property table (props) is already bound
                                transform = function( value, _ )
                                    if value == "one" then
                                        return false
                                    else
                                        return true
                                    end
                                end
                            }),
                    }
                        
                },           
            }

            local result = LrDialogs.presentModalDialog({
                alignment = "center",
                title = "Face Recognition Modul",
                contents = c,
                actionVerb = "Detect"
                }
            )
            

            function setCroppedDimensions(i)
                    outputToLog("setCrop: ")
                    outputToLog(i)
                    photo = selectedPhotos[i]

                    par = {timeout=2, callback=LrTasks.sleep(0.1), asynchronus=false}
                    if photo then
                        catalog:withWriteAccessDo('Set Crop', writeCrop, par)                        
                   end
            end

            function writeCrop()
                            outputToLog("Croping: ")
                            photo:applyDevelopSettings({CropRight = tonumber(detectedValues[1])})
                            photo:applyDevelopSettings({CropLeft = tonumber(detectedValues[2])})
                            photo:applyDevelopSettings({CropTop = tonumber(detectedValues[3])})
                            photo:applyDevelopSettings({CropBottom = tonumber(detectedValues[4])})
                           --photo:applyDevelopSettings({CropAngle =-tonumber(detectedValues[5])})
                           -- Vycisti tabulku
                           outputToLog(tonumber(detectedValues[1]))
                           outputToLog(tonumber(detectedValues[2]))
                           outputToLog(tonumber(detectedValues[3]))
                           outputToLog(tonumber(detectedValues[4]))
                           outputToLog("done.")
                            for k in pairs (detectedValues) do
                                detectedValues [k] = nil
                            end
                            

            end
            

            local function detectFaces(i,dataTable)
                catalog = LrApplication.activeCatalog()
                selectedPhotos = catalog:getTargetPhotos()
                path = LrPathUtils.child(LrPathUtils.child(LrPathUtils.child(LrPathUtils.child(LrPathUtils.parent(_PLUGIN.path), "modul_console_app"), "modul"), "x64"), "Release")
                picturePath = folderNames[i]
                commandParams = "width" .. '" "' .. dataTable[2] .. '" "' .. "height" .. '" "' .. dataTable[3] .. '" "' .. "ppi" .. '" "' .. dataTable[4] .. '" "' .. "headHeight" .. '" "' .. dataTable[5] .. '" "' .. "topMargin" .. '" "' .. dataTable[6] .. '" "' .. "bottomToEye" .. '" "' .. dataTable[7] .. '" "' .. "faceWidth" .. '" "' .. dataTable[8] .. '" "' .. "bottomToChin" .. '" "' .. dataTable[9] .. '" "' .. "faceArea" .. '" "' .. dataTable[10] .. '" "' .. "bottomToEyeArea" .. '" "' .. dataTable[11] ..'" "' .. "topMarginArea" .. '" "'.. dataTable[12]
                command = '"' .. LrPathUtils.child(path, "modul.exe" ) .. '" ' .. '"' .. picturePath .. '" ' .. '"' .. "ratio" .. '" "' .. "1.1" .. '" "' .. commandParams .. '"'
				quotedCommand = '"'.. command .. '"'
                outputToLog("Command poslany do konzolovej aplikacie: " .. quotedCommand)
                    
                if LrTasks.execute( quotedCommand ) == 1 then
                    file = io.open(path .. "\\output.txt", "r")
                    for line in file:lines() do
                        table.insert (detectedValues, line)
                    end
                    io.close(file)
                    orient = selectedPhotos[i]:getDevelopSettings('Orientation')
                    outputToLog("Orient:")
                    outputToLog(orient)
                    setCroppedDimensions(i)


                elseif LrTasks.execute( quotedCommand ) == 0 then
                    LrDialogs.message("Nesprávne zadané hodnoty", "prosím skúste zadať znova")
                    outputToLog("Je tam chyba!")
                end
                
            end


            -- V prípade že užívateľ klikol na tlačitko DETECT ulož postupne všetky vpísané dáta do tabulky 
            local dataTable ={}
            if result == 'ok' then
                selection = props.selectedButton
                if selection == "one" then 
                    c = popupCountry.value  
                    dataTable[1] = popupRatio.value
                    dataTable[2] = photoCountryParams[tonumber(c)].width
                    dataTable[3] = photoCountryParams[tonumber(c)].height          
                    dataTable[4] = photoCountryParams[tonumber(c)].ppi              
                    dataTable[5] = photoCountryParams[tonumber(c)].headHeight      
                    dataTable[6] = photoCountryParams[tonumber(c)].topMargin          
                    dataTable[7] = photoCountryParams[tonumber(c)].bottomToEye     
                    dataTable[8] = photoCountryParams[tonumber(c)].faceWidth          
                    dataTable[9] = photoCountryParams[tonumber(c)].bottomToChin  
                    dataTable[10] = photoCountryParams[tonumber(c)].faceArea          
                    dataTable[11] = photoCountryParams[tonumber(c)].bottomToEyeArea  
                    dataTable[12] = photoCountryParams[tonumber(c)].topMarginArea
                    --údaj v prípade že vyberie automatickú možnosť
                elseif selection == "two" then
                    country = "0" 
                    dataTable[1] = popupRatio.value             --prvá pozícia v tabulke pre POMER STRAN
                    dataTable[2] = setWidth.value               --druhá pozícia v tabulke je ŠÍRKA FOTOGRAFIE
                    dataTable[3] = setHeight.value              --tretia pozícia v tabulke je VÝŠKA FOTOGRAFIE
                    dataTable[4] = setDPI.value                 --štvrtá pozícia v tabulke je ROZLÍŠENIE FOTOGRAFIE (DPI)
                    dataTable[5] = setHeadHeight.value          --piata pozícia v tabulke je VÝŠKA HLAVY - BRADA -> HLAVA
                    dataTable[6] = setTopMargin.value           --šiesta pozícia v tabulke je pre VZDIALENOSŤ - HLAVA -> HORNÝ OKRAJ
                    dataTable[7] = setBottomEyeHeight.value     --siedma pozícia v tabulke je pre VZDIALENOSŤ - LÍNIA OČÍ -> DOLNÝ KRAJ
                    dataTable[8] = setFaceWidth.value           --ôsma pozícia v tabulke je pre VZDIALENOSŤ - ĽAVÝ KRAJ TVÁRE -> PRAVÝ KRAJ TVÁRE
                    dataTable[9] = setBottomChinHeight.value    --deviata pozícia v tabulke je pre VZDIALENOSŤ - - DOLNÝ KRAJ -> BRADA
                    dataTable[10] = setFaceArea.value           --desiata pozícia v tabulke je pre hodnotu: PLOCHA HLAVY
                    dataTable[11] = setBottomEyeArea.value      --jedenásta pozícia v tabulke je pre hodnotu: PLOCHA DOLNÝ KRAJ -> LÍNIA OČÍ 
                    dataTable[12] = setHeadTopArea.value        --dvanásta  pozícia v tabulke je pre hodnotu: PLOCHA NAD HLAVOU PO OKRAJ
                end
                for i in pairs ( selectedPhotos ) do
                    detectFaces(i, dataTable)
                    
                end
                LrDialogs.message("Fotografie boli orezané")
            end

           
        end)       
        
        
        --io.output("C:\\Users\\micha\\OneDrive - Mendelova univerzita v Brně\\DP\\Praktická časť\\Kód\\face_recognition.lrplugin\\test.txt"):write(fName)
 --       io.close()
        
end

--[[
- čítanie zo súboru do stringu
LrFileUtils.readFile("C:\\Users\\micha\\OneDrive - Mendelova univerzita v Brně\\DP\\Praktická časť\\Kód\\face_recognition.lrplugin\\test.txt")
]]
-- Spustenie funkcie showCustomDialog stým že súbežne vznikne nové vlákno procesu popri hlavnom vlákne Lightroomu
import 'LrTasks'.startAsyncTask( showCustomDialog  )
