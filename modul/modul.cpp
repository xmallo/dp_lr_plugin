﻿// modul.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#define _USE_MATH_DEFINES

#include <cmath>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/face.hpp>
#include <iostream>
#include <fstream>
#include <Magick++.h>
#include <opencv2/core/utility.hpp>

using namespace cv;
using namespace std;
using namespace Magick;
using namespace cv::face;


//rotate picture
Mat rotate(Mat src, double angle)                           //rotate function returning mat object with parametres imagefile and angle    
{
    Mat dst;                                                //Mat object for output image file
    Point2f pt(src.cols / 2., src.rows / 2.);               //point from where to rotate    
    Mat r = getRotationMatrix2D(pt, angle, 1.0);            //Mat object for storing after rotation
    warpAffine(src, dst, r, Size(src.cols, src.rows));      //applie an affine transforation to image.
    return dst;                                             //returning Mat object for output image file
}

//ppi= 300 | ppcm = 118,11 |||| ppi = 600 | ppcm = 236,22
float pixelToCm(int ppi, int length) {
    float pxmm;
    float cm;
    pxmm = 1 / (ppi / 25.4);              //ppi prepocet na px/mm
    cm = (pxmm * length) / 10;        //sirka na cm

    return cm;
}

int mmToPixel(int ppi, float length) {
    int pix = (length / 25.4)* ppi;
    return pix;
}

// funkcia na vypocet kolko pixelov potrbujes na zaklade plochy
float calculateHeight(int width, int height, float percentage) {
    float x;
    x = ((width * height)/(100/percentage))/width;
    return x;
}

//obsah plochy
float calculateArea(int width, int height) {
    float area = width * height;
    return area;
}

int main(int argc, char** argv)
{
    int e1, e2;
    float t;
    cout << "argv 0 je: " << argv[0] << "\n"; //upravit string argv 0 + model
    

    CascadeClassifier faceDetector("C:\\detector\\modul_console_app\\modul\\x64\\Release\\haarcascade_frontalface_alt2.xml");

    Ptr<Facemark> facemark = FacemarkLBF::create();
    facemark->loadModel("C:\\detector\\modul_console_app\\modul\\x64\\Release\\lbfmodel.yaml");

    InitializeMagick(*argv);

    cout << "You have entered " << argc << " arguments:" << "\n";
    // cesta k fotke
    string photoPath = argv[1];
    float ratio = 0, width = 0, height = 0, headHeight = 0, topMargin = 0, bottomToEye = 0, faceWidth = 0, bottomToChin = 0, faceArea = 0, bottomToEyeArea = 0, topMarginArea = 0;
    int ppi = 0;


    
    for (int i = 2; i < argc; i += 2) //cyklus na ulozenie parametrov zo vstupu
    {
        string param = argv[i];
        float value = std::stof(argv[i + 1]);
        if (param == "ratio") {
            ratio = value;
        }
        else if (param == "width") {
            width = value;
        }
        else if (param == "height") {
            height = value;
        }
        else if (param == "ppi") {
            ppi = value;
        }
        else if (param == "headHeight") {
            headHeight = value;
        }
        else if (param == "topMargin") {
            topMargin = value;
        }
        else if (param == "bottomToEye") {
            bottomToEye = value;
        }
        else if (param == "faceWidth") {
            faceWidth = value;
        }
        else if (param == "bottomToChin") {
            bottomToChin = value;
        }
        else if (param == "faceArea") {
            faceArea = value;
        }
        else if (param == "bottomToEyeArea") {
            bottomToEyeArea = value;
        }
        else if (param == "topMarginArea") {
            topMarginArea = value;
        }
    }
    

  
    Image image;
    image.read(photoPath);              //načítam obrázok (imageMagick)
    int w = image.columns();            
    int h = image.rows();       
    Mat img(h, w, CV_8UC3);             // vytvorím prázdnu maticu o výške ašírke foto (opencv)
    Mat imgOut;                         
    image.write(0, 0, w, h, "BGR", Magick::CharPixel, img.data);    //metóda imageMagick -- prepis z imageMagick do opencv aby mal formát mat
    
    int imgWidth = img.size().width;
    int imgHeight = img.size().height;
    //cv::resize(imgIn, img, imgIn.rows / 2., imgIn.cols / 2.);

    if (img.empty())
    {
        cout << "Could not read the image: " << photoPath << std::endl;
        //return 1;
    }
    
    // e1 = getTickCount();
    std::vector<cv::Rect> facesVect;                //vektor rectanglov tvárí
    Rect face;                                      //jeden rectangle tváre
    vector< vector<Point2f> > landmarksVect;        //vektor vektorov bodov tváre
    vector<Point2f> landmarks;                      //keby bolo viac, tak aby som nakoniec mal len jednu správnu-- vektor bodov tváre
    cv::Mat gray;                                   //matica na šedotónový obraz                 
    cv::cvtColor(img, gray, COLOR_BGR2GRAY);        // prefarbenie obrázka na šedotón
    e1 = getTickCount();
    faceDetector.detectMultiScale(gray, facesVect); //uloženie tvárí do vektoru rektanglov
    e2 = getTickCount();
    t = (e2 - e1) / getTickFrequency();
    //    cout << "Doba: " << t << "\n";
    //
   
    //e1 = getTickCount();
    
    bool success = facemark->fit(img, facesVect, landmarksVect);    
    
    //e2 = getTickCount();
    //t = (e2 - e1) / getTickFrequency();
    
    Point2f leftEye, rightEye, chin, rightOfChin, leftOfChin, middleOfEye, leftEar, rightEar, nose;
    if (success)
    {      
        for (int i = 0; i < facesVect.size(); i++)
        {
            if (facesVect[i].width > (imgWidth/4)) {
                face = facesVect[i];
                landmarks = landmarksVect[i];
            }
        }
    }
    cv::rectangle(img, face, Scalar(255, 0, 0));
   
    
    
    for (int j = 0; j < landmarks.size(); j++) {
        cv::circle(img, landmarks[j], 5, Scalar(0, 0, 255), FILLED);
        
    }
   
    cv::line(img, landmarks[36], landmarks[45], Scalar(255, 0, 0), 2, 8, 0);
    
    leftEye = landmarks[36];
    rightEye = landmarks[45];
    middleOfEye = landmarks[27];
    leftEar = landmarks[0];
    rightEar = landmarks[16];
    chin = landmarks[8];
    nose = landmarks[30];
    

    // vykresli ciaru cez bradu
    leftOfChin = Point2f(0., chin.y);
    rightOfChin = Point2f(img.size().height, chin.y);
    line(img, leftOfChin, rightOfChin, Scalar(255, 0, 0), 2, 8, 0);

    //vypocet uhla linie medzi ocami    
    double angleRadians = atan2(leftEye.y - rightEye.y, rightEye.x - leftEye.x);
    double angle = angleRadians * 180 / M_PI;
    
    
    /* GrabCut example
    Mat mask, bgdModel, fgdModel;
    grabCut(img, mask, faces[0], bgdModel, fgdModel, 5, GC_INIT_WITH_RECT);

    cv::compare(mask, cv::GC_FGD, mask, cv::CMP_EQ);
    cv::Mat foreground(img.size(), CV_8UC3, cv::Scalar(255, 255, 255));
    img.copyTo(foreground, ~mask); // bg pixels not copied
    */
    Mat dst, edges;
    int topOfHead;

    Canny(img, edges, 100, 200, 3); //detektor hrán z nastavením prahov(citlivost)  
    for (int i = 0; i < img.size().height; i++) {
        int suma = 0;

        for (int j = 0; j < img.size().width; j++) {
            suma += edges.at<uchar>(i,j);       //at.<uchar> pristup k pixelu
        }
        if (suma > 0) {
            topOfHead = i;              //vráti výšku
            break;
        }
    }
    
    //vykreslenie línie vrch hlavy
    Point2f leftTopOfHead, rightTopOfHead;
    leftTopOfHead = Point2f(0., topOfHead);
    rightTopOfHead = Point2f(img.size().width, topOfHead);
    line(img, leftTopOfHead, rightTopOfHead, Scalar(255, 0, 0), 2, 8, 0);
    cout << "Vrchny bod: " << topOfHead << "\n";
    cout << "Brada bod: " << chin.y << "\n";
    cout << "Hlava: " << (chin.y - topOfHead) << "\n";

    
    float headRatio, ratioTopMargin, newRatio, cutTop, cutBottom, cutWidth, ratioEyeBottom, onePercent, chinToEye, headInPixels, bottomLine, ratioFaceWidth, checkFaceWidth, cutLeft, cutRight;
    
    // 1 moznost parametrov ( vyska, sirka, vyska hlava)
    if (!topMarginArea && !bottomToEyeArea && !faceArea && !bottomToChin && !faceWidth && !bottomToEye && !topMargin) {
        cout << "Zadana je width, height, headHeight" << "\n";
        headRatio = headHeight / height;
        ratioTopMargin = (0.5 * (height - headHeight)) / height;
        newRatio = width / height;

        cutTop = topOfHead - (((chin.y - topOfHead) / (headRatio * 100))*(ratioTopMargin*100)); // odrezat od vrcholu fotky
        cutBottom = imgHeight - (chin.y + ((chin.y - topOfHead) / (headRatio * 100)) * (((height - topMargin - headHeight) / height) * 100)); // oderazt od spodku fotografie
        cutWidth = imgWidth - ((imgHeight - cutBottom - cutTop) * newRatio);
    }
    // 2 moznost parametrov ( vyska, sirka, plocha hlavy)
    else if (!topMarginArea && !bottomToEyeArea && !headHeight && !bottomToChin && !faceWidth && !bottomToEye && !topMargin) {   
        cout << "Zadana je width, height, faceArea" << "\n";
        headRatio = faceArea / 100; 
        ratioTopMargin = (0.5*(1-headRatio));
        newRatio = width / height;

        cutTop = topOfHead - ((chin.y - topOfHead) / faceArea) * (ratioTopMargin * 100);
        cutBottom = imgHeight - (chin.y + ((chin.y - topOfHead) / faceArea) * (ratioTopMargin * 100));
        cutWidth = imgWidth - ((imgHeight - cutBottom - cutTop) * newRatio);
    }
    // 3 moznost parametrov ( vyska, sirka, vyska hlavy, vyska nad hlavou)
    else if (!topMarginArea && !bottomToEyeArea && !faceArea && !bottomToChin && !faceWidth && !bottomToEye) {
        cout << "Zadana je width, height, headHeight, topMargin" << "\n";
        
        //pomer výšky hlavy na fotofrafii
        headRatio = headHeight / height;
        //pomer vzdialenosti hlava - vrch fotky
        ratioTopMargin = topMargin / height;
        //pomer fotografie( zadany parameter ratio)
        newRatio = width / height;

        //výpočet orezov fotografie
        cutTop = topOfHead - ((chin.y - topOfHead) / (headRatio*100)) * (ratioTopMargin * 100); // odrezat od vrcholu fotky
        cutBottom = imgHeight - (chin.y + ((chin.y - topOfHead) / (headRatio * 100)) * (((height - topMargin - headHeight) / height) * 100)); // oderazt od spodku fotografie
        cutWidth = imgWidth - ((imgHeight - cutBottom - cutTop) * newRatio);
       //cutTop = topOfHead * (ratioTopMargin - ratioTopMargin / headRatio + 1) + chin.y * (ratioTopMargin - ratioTopMargin / headRatio) - ratioTopMargin * imgHeight;
       //cutBottom = imgHeight - cutTop - 1 / ratioTopMargin * topOfHead + 1 / ratioTopMargin * cutTop;
       //cutWidth = -newRatio / 2 * imgHeight + 1 / 2 + cutTop + 1 / 2 * cutBottom + 1 / 2 * imgWidth;
       
    }
    // 4 moznost parametrov ( vyska, sirka, plocha hlavy, vyska nad hlavou)
    else if (!topMarginArea && !bottomToEyeArea && !headHeight && !bottomToChin && !faceWidth && !bottomToEye) {
        cout << "Zadana je width, height, faceArea, topMargin" << "\n";
        headRatio = faceArea / 100;
        ratioTopMargin = topMargin / height;
        newRatio = width / height;

        cutTop = topOfHead - ((chin.y - topOfHead) / faceArea) * (ratioTopMargin * 100);
        cutBottom = imgHeight - (chin.y + ((chin.y - topOfHead) / faceArea) * (100 - faceArea - (ratioTopMargin * 100)));
        cutWidth = imgWidth - ((imgHeight - cutBottom - cutTop) * newRatio);
    }
    // 5 moznost parametrov ( vyska, sirka, plocha hlavy, plocha nad hlavou)
    else if (!topMargin && !bottomToEyeArea && !headHeight && !bottomToChin && !faceWidth && !bottomToEye) {
        cout << "Zadana je width, height, faceArea, topMarginArea" << "\n";
        headRatio = faceArea / 100;
        ratioTopMargin = topMarginArea / 100;
        newRatio = width / height;

        cutTop = topOfHead - ((chin.y - topOfHead) / faceArea) * topMarginArea;
        cutBottom = imgHeight - (chin.y + ((chin.y - topOfHead) / faceArea) * (100 - faceArea - topMarginArea));
        cutWidth = imgWidth - ((imgHeight - cutBottom - cutTop) * newRatio);
    }
    // 6 moznost parametrov ( vyska, sirka, oci od spodku, vyska nad hlavou)
    else if (!topMargin && !bottomToEyeArea && !topMarginArea && !bottomToChin && !faceWidth && !faceArea) {
        cout << "Zadana je width, height, bottomToEye, headHeight" << "\n";
        headRatio = headHeight / height;
        ratioEyeBottom = bottomToEye / height;
        newRatio = width / height;
        headInPixels = chin.y - topOfHead; //pocet pixelov kolko zabera hlava
        onePercent = headInPixels / (headRatio * 100); // 1 percento zaberá X pixelov
        bottomLine = middleOfEye.y + (onePercent * (ratioEyeBottom * 100)); //y suradnica kde ma byt spodok na zaklade bottomToEye


        cutBottom = imgHeight - bottomLine; //odrez od spodnej casti foto
        cutTop = bottomLine - (onePercent*100); // vypocet horneho rezu = spodna linia - kolko ma zaberat cela fotka
        cutWidth = imgWidth - ((imgHeight - cutBottom - cutTop) * newRatio);
    }
    // 7 moznost parametrov ( vyska, sirka, plocha oci od spodku, plocha hlavy)
    else if (!topMargin && !bottomToEye && !topMarginArea && !bottomToChin && !faceWidth && !headHeight) {
        cout << "Zadana je width, height, bottomToEyeArea, faceArea" << "\n";
        headRatio = faceArea / 100;
        ratioEyeBottom = bottomToEyeArea / 100;
        newRatio = width / height;
        headInPixels = chin.y - topOfHead; //pocet pixelov kolko zabera hlava
        onePercent = headInPixels / faceArea; // 1 percento zaberá X pixelov
        bottomLine = middleOfEye.y + (onePercent * bottomToEyeArea); //y suradnica kde ma byt spodok na zaklade bottomToEye


        cutBottom = imgHeight - bottomLine; //odrez od spodnej casti foto
        cutTop = bottomLine - (onePercent * 100); // vypocet horneho rezu = spodna linia - kolko ma zaberat cela fotka
        cutWidth = imgWidth - ((imgHeight - cutBottom - cutTop) * newRatio);
    }
    // 8 moznost parametrov ( vyska, sirka, oci od spodku, vyska hlavy, sirka hlavy
    else if (!faceWidth && !bottomToEyeArea && !topMarginArea && !bottomToChin && !faceArea) {
        cout << "Zadana je width, height, bottomToEye, topMargin, headHeight " << "\n";
        headRatio = headHeight / height;
        ratioEyeBottom = bottomToEye / height;
        ratioTopMargin = topMargin / height;
        newRatio = width / height;


        cutTop = topOfHead - ((chin.y - topOfHead) / (headRatio * 100)) * (ratioTopMargin * 100); // odrezat od vrcholu fotky
        cutBottom = imgHeight - (chin.y + ((chin.y - topOfHead) / (headRatio * 100)) * (((height - topMargin - headHeight) / height) * 100)); // oderazt od spodku fotografie
        cutWidth = imgWidth - ((imgHeight - cutBottom - cutTop) * newRatio);
    }
    // 9 moznost parametrov ( vyska, sirka, oci od spodku, vyska hlavy, sirka hlavy)
    else if (!bottomToEyeArea && !topMarginArea && !bottomToEye && !faceArea) {
        cout << "Zadana je width, height, bottomToChin, topMargin, headHeight, faceWidth " << "\n";
        headRatio = headHeight / height;
        ratioEyeBottom = bottomToEye / height;
        ratioTopMargin = topMargin / height;
        ratioFaceWidth = faceWidth / width;
        newRatio = width / height;

        // ked to spravne vypocita vysku a top margin ... tak spodok a sirka sedi

        cutTop = topOfHead - ((chin.y - topOfHead) / (headRatio * 100)) * (ratioTopMargin * 100); // odrezat od vrcholu fotky
        cutBottom = imgHeight - (chin.y + ((chin.y - topOfHead) / (headRatio * 100)) * (((height - topMargin - headHeight) / height) * 100)); // oderazt od spodku fotografie
        cutWidth = imgWidth - ((imgHeight - cutBottom - cutTop) * newRatio);
    }
    // 10 |||### ZATIAL LEN E-VIZA CINA, NEFUNGUJE####|||| moznost parametrov ( vyska, sirka, oci od spodku, vyska hlavy, sirka hlavy) 
    else if (!topMargin && !bottomToEyeArea && !topMarginArea && !bottomToChin && !faceArea) {
    cout << "Zadana je width, height, bottomToEye, faceWidth, headHeight " << "\n";
    headRatio = headHeight / height;
    ratioEyeBottom = bottomToEye / height;

    newRatio = width / height;
    headInPixels = chin.y - topOfHead; //pocet pixelov kolko zabera hlava
    onePercent = headInPixels / (headRatio * 100); // 1 percento zaberá X pixelov
    bottomLine = middleOfEye.y + (onePercent * (ratioEyeBottom * 100)); //y suradnica kde ma byt spodok na zaklade bottomToEye


    cutBottom = imgHeight - bottomLine; //odrez od spodnej casti foto
    cutTop = bottomLine - (onePercent * 100); // vypocet horneho rezu = spodna linia - kolko ma zaberat cela fotka
    cutWidth = imgWidth - ((imgHeight - cutBottom - cutTop) * newRatio);
    }
    
    // ZLE ZADANE HODNOTY
    else {
        cout << "zle zadane kombinacia parametrov" << "\n";

        return 0;
    }
    
    /*
    *   *** Bacha Lightroom ma nastavené vrcholy podla fotografie na sirku
    *       - to znamena moj vrch je v skutocnosti prava strana atd.
    * 
    * 
    *                                               CutLeft
    *    ______________                             _______
    *   |              |                           |       |
    *   |              |      ====>       CutBottom|       |CutTop
    *   |______________|                           |_______|
    *                                              CutRight
    */
    float cTop, cBottom, nosX, remainingWidth, halfOfRemaningWidth, newHeight;
    // prepocet na pomer podla ktoreho potom fotografou oreze lightroom
    // (1-) je tam pretoze sa interval (0-1) podla ktoreho orezava. Ked chcem orezat z prava treba dat 1-
    cTop = 1 - (cutTop / imgHeight);
    cBottom = cutBottom / imgHeight;
    nosX = round(nose.x);
    remainingWidth = imgWidth - cutWidth;
    halfOfRemaningWidth = remainingWidth / 2;
    newHeight = imgHeight - (cutTop + cutBottom);


        // vypocet skratenia krajov podla toho kde má byt stred 
        // vypocet orezu z pravej strany, pomerovo k stredu fotky(nos)
        // vysledok odratame od jednotky pretoze orezavame z prava( pocita sa od lavej strany)
        cutRight = 1 - ((imgWidth - (nosX + halfOfRemaningWidth)) / imgWidth);
        // vypocet orezu z lavej strany, pomerovo k stredu foto(nos)
        cutLeft = ((nosX - halfOfRemaningWidth) / imgWidth);
      
   /*Mat res;
     Size size(w / 6., h / 6.);
     cv::resize(img, res,  size);
     imshow("Display window", res);
     int k = waitKey(0); // Wait for a keystroke in the window
        if (k == 's')
        {
            imwrite("starry_night.png", img);
        }
     

        cout << "Cut top: " << cutTop << "\n";
        cout << "Cut top: " << cTop << "\n";
        cout << "New Height: " << newHeight << "\n";
        cout << "Cut bottom: " << cutBottom << "\n";
        cout << "Cut left: " << cutLeft<< "\n";
        cout << "Cut right: " << cutRight<< "\n";
        */
    fstream file("C:\\detector\\modul_console_app\\modul\\x64\\Release\\output.txt");
    file << cTop << endl;
    file <<  cBottom << endl;
    file <<  cutLeft << endl;
    file <<  cutRight << endl;
    file <<  angle << endl;
    file.close();
    cout << "Doba: " << t << "\n";
    return 1;
}